'use strict'

const util = require('util')
const mysql = require('mysql')
const db = require('../configs/db')

module.exports = {
    get: (req, res) => {
        let sql = 'SELECT * FROM student'
        db.query(sql, (err, esponse)=>{
            if (err) throw err
            res.json(esponse)
        })
    },
    detail: (req, res) => {
        let sql = 'SELECT * FROM student WHERE id=?'
        let studentId = req.params.studentId;
        db.query(sql, studentId, (err, response) =>{
            if (err) throw err
            res.json(response[0])
        })
    },
    update: (req, res) =>{
        let {userName, password, name, code}= req.body;
        let studentId = req.params.studentId;
        let sql = 'UPDATE student SET userName = (?), password = md5(?), name=(?) , code=(?) WHERE id = ?'
        db.query(sql, [userName, password, name, code, studentId], (err, response)=>{
            if (err) throw err
            res.json({message: 'Update success!'})
        })
    },
    store: (req, res)=>{
        let data = req.body;
        let sql = 'INSERT INTO student SET ?'
        db.query(sql, [data], (err, response) =>{
            if (err) throw err
            res.json({message: 'Insert success!'})
        })
    },
    delete: (req, res) => {
        let sql = 'DELETE FROM student WHERE id =?'
        db.query(sql, [req.params.studentId], (err, response) =>{
            if (err) throw err
            res.json({message: 'Delete success!'})
        })
    }
}