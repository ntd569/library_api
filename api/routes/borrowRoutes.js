'use strict';
const express = require('express')
const router = express.Router()

let borrowsControllers = require('../controllers/borrowsControllers')
const {isLogedIn} =  require("../middlewares/authencationMiddlewares")
const {loginuser} = require('../middlewares/loginstudentsMiddlewares')

router.get('/student/borrow',loginuser,borrowsControllers.get)
router.get('/student/borrow/:borrowId',loginuser,borrowsControllers.detail)

router.put('/admin/return/borrow/:borrowId',isLogedIn,borrowsControllers.return)
router.get('/admin/borrow',isLogedIn,borrowsControllers.get)
router.get('/admin/borrow/:borrowId',isLogedIn,borrowsControllers.detail)
router.post('/admin/borrow',isLogedIn,borrowsControllers.store)
router.put('/borrow/:borrowId',isLogedIn,borrowsControllers.update)
router.delete('/borrow/:borrowId',isLogedIn,borrowsControllers.delete);

module.exports = router