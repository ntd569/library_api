'use strict'

const util = require('util')
const mysql = require('mysql')
const db = require('../configs/db')

module.exports = {
    get: (req, res) => {
        let sql = 'SELECT * FROM publishers'
        db.query(sql, (err, esponse)=>{
            if (err) throw err
            res.json(esponse)
        })
    },
    detail: (req, res) => {
        let sql = 'SELECT * FROM publishers WHERE id=?'
        let publisherId = req.params.publisherId;
        db.query(sql, publisherId, (err, response) =>{
            if (err) throw err
            res.json(response[0])
        })
    },
    update: (req, res) => {
        let data = req.body;
        let publisherId = req.params.publisherId;
        let sql = 'UPDATE publishers SET ? WHERE id = ?'
        db.query(sql, [data, publisherId], (err, response) => {
            if (err) throw err
            res.json({message: 'Update success!'})
        })
    },
    store: (req, res) => {
        const data = req.body;
        let sql = 'INSERT INTO publishers SET ?'
        db.query(sql, [data], (err, response) => {
            if (err) throw err
            res.json({message: 'Insert success!'})
        })
    },
    delete: (req, res) => {
        let sql = 'DELETE FROM publishers WHERE id = ?'
        let publisherId = req.params.publisherId;
        db.query(sql, [publisherId], (err, response) =>{
            if (err) throw err
            res.json({message: 'Delete success!'})
        })
    }
}