'use strict';
const express = require('express')
const router = express.Router()
var cloudinary = require('cloudinary');
var cloudinaryStorage = require('multer-storage-cloudinary');
var multer = require('multer');
const db = require('../configs/db');
let imageControllers = require('../controllers/imageControllers')
const  { isLogedIn } =  require("../middlewares/authencationMiddlewares")

cloudinary.config({
  cloud_name: 'qltv-com',
  api_key: '734172897626831',
  api_secret: 'fcBqOgD34c6SyY4tRGHKpfjWLy4'
});

const storage = cloudinaryStorage({
    cloudinary: cloudinary,
    folder: 'qltv',
    allowedFormats: ['jpg', 'png'],
    filename: function (req, file, cb) {
      cb(undefined, file.originalname);
    }
  });
  var parser = multer({ storage: storage });
  router.put('/upload/image/:bookId',isLogedIn,parser.single('image'), function (req, res) { 
    let bookId = req.params.bookId
    let url = req.file.url
    let sql = 'UPDATE books SET image = ? WHERE id = ?'
    db.query(sql, [url, bookId],(err,response)=>{
        if (err) throw err
        res.json({message: 'Updata image success!'})
    })
});


module.exports = router