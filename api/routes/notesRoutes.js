'use strict';
const express = require('express')
const router = express.Router()

let notesControllers = require('../controllers/notesControllers')
const {isLogedIn} =  require("../middlewares/authencationMiddlewares")
const {loginuser} = require('../middlewares/loginstudentsMiddlewares')

router.get('/student/notes',loginuser,notesControllers.get)
router.get('/student/notes/:noteId',loginuser,notesControllers.detail)

router.get('/admin/notes',isLogedIn,notesControllers.get)
router.get('/admin/notes/:noteId',isLogedIn,notesControllers.detail)
router.post('/admin/notes',isLogedIn,notesControllers.store)
router.put('/notes/:noteId',isLogedIn,notesControllers.update)
router.delete('/notes/:noteId',isLogedIn,notesControllers.delete);

module.exports = router