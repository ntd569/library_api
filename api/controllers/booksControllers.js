'use strict'

const util = require('util')
const mysql = require('mysql')
const db = require('../configs/db')

module.exports = {
    get: (req, res) => {
        let sql = 'SELECT * FROM books name like ?'
        db.query(sql, [ `%${req.query.search ? req.query.search.trim():""}%` ],(err, response) => {
            if (err) throw err
            res.json(response)
        })
    },
    detail: (req, res) => {
        let bookId = req.params.bookId
        let sql ="SELECT * FROM books WHERE ?"
        db.query(sql, bookId, (err, response) => {
            if (err) throw err
            res.json(response[0])
        })
    },
    update: (req, res) => {
        let data = req.body;
        let bookId = req.params.bookId;
        let sql = 'UPDATE books SET ? WHERE id = ?'
        db.query(sql, [data, bookId], (err, response) => {
            if (err) throw err
            res.json({message: 'Update success!'})
        })
    },
    store: (req, res) => {
        let data = req.body;
        let sql = 'INSERT INTO books SET ?'
        db.query(sql, [data], (err, response) => {
            if (err) throw err
            res.json({message: 'Insert success!'})
        })
    },
    delete: (req, res) => {
        let sql = 'DELETE FROM books WHERE id = ?'
        db.query(sql, [req.params.bookId], (err, response) => {
            if (err) throw err
            res.json({message: 'Delete success!'})
        })
    }
}