const jwt = require('jsonwebtoken');
const client = require('../configs/db')
module.exports.login = (req, res) => {
    const { username, password } = req.body;
    client.query("SELECT * FROM users " +
            "WHERE users.userName = ? AND users.password = md5(?)",[username, password], function(err, result) {
        if (err) {
            res.status(403).send('Invalid username or password');
        } else {
            console.log(result)
            if (result.length === 0) {
                res.status(403).send('Invalid username or password');
            } else {
                const { id, userName } = result[0]
                var tokenadmin = jwt.sign({
                    id, userName
                }, process.env.SECRET_KEY1, {algorithm: 'HS256', expiresIn: '2h'});
                res.json({ tokenadmin });
            }
           
        }
    })
}
