'use strict';
const express = require('express')
const router = express.Router()

let authorsControllers = require('../controllers/authorsControllers')
const {isLogedIn} =  require("../middlewares/authencationMiddlewares")
const {loginuser} = require('../middlewares/loginstudentsMiddlewares')

router.get('/student/authors',loginuser,authorsControllers.get)
router.get('/student/authors/:authorId',loginuser,authorsControllers.detail)

router.get('/admin/authors',isLogedIn,authorsControllers.get)
router.get('/admin/authors/:authorId',isLogedIn,authorsControllers.detail)
router.post('/admin/authors',isLogedIn,authorsControllers.store)
router.put('/authors/:authorId',isLogedIn,authorsControllers.update)
router.delete('/authors/:authorId',isLogedIn,authorsControllers.delete);

module.exports = router