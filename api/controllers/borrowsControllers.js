'use strict'

const util = require('util')
const mysql = require('mysql')
const db = require('../configs/db')
var date = new Date();
module.exports = {
    get: (req, res) => {
        let sql = 'SELECT * FROM borrow'
        db.query(sql, (err, response) => {
            if (err) throw err
            res.json(response)
        })
    },
    detail: (req, res) => {
        let sql = 'SELECT * FROM borrow WHERE id= ?'
        db.query(sql, [req.params.borrowId], (err, response) => {
            if (err) throw err
            res.json(response[0])
        })
    },
    update: (req, res) => {
        let {studentCode, bookId, borrowDate, bookLender} = req.body;
        let sql = 'UPDATE borrow SET studentCode = (?), bookId = (?), borrowDate = (?), returnDate = ?, bookLender = ? WHERE id = ?'
        db.query(sql, [studentCode, bookId, borrowDate, returnDate, bookLender, borrowId], (err, response) => {
            console.log(date, studentCode, bookId, borrowDate, returnDate, bookLender)
            if (err) throw err
            res.json({message: 'Update success!'})
        })
    },
    return: (req, res) => {
        let returnDate = date;
        let borrowId = req.params.borrowId;
        let sql = 'UPDATE borrow SET returnDate = ? WHERE id =?'
        db.query(sql, [returnDate,borrowId], (err, response) => {
            console.log(returnDate)
            if (err) throw err
            res.json({message: 'Update success!'})
        })
    },
    store: (req, res) => {
        let {studentCode, bookId, bookLender} = req.body;
        let borrowDate = date;
        let sql = 'INSERT INTO borrow SET studentCode = ?, bookId = ?,  bookLender = ?,  borrowDate = ?'
        db.query(sql, [studentCode, bookId, bookLender, borrowDate,], (err, response) => {
            if (err) throw err
            res.json({message: 'Insert success!'})
        })
    },
    delete: (req, res) => {
        let sql = 'DELETE FROM borrow WHERE id = ?'
        db.query(sql, [req.params.borrowId], (err, response) => {
            if (err) throw err
            res.json({message: 'Delete success!'})
        })
    }
}