const express = require('express')
const router = express.Router()

let addstudentsController = require('../controllers/addstudentsController')

router.post('/user',addstudentsController.store)

module.exports = router