'use strict';
const express = require('express')
const router = express.Router()

let usersControllers = require('../controllers/usersControllers');
const  { isLogedIn } =  require("../middlewares/authencationMiddlewares")

router.get('/users',isLogedIn,usersControllers.get)
router.post('/users',isLogedIn,usersControllers.store);
  
router.get('/users/:usersId',isLogedIn,usersControllers.detail)
router.put('/users/:usersId',isLogedIn,usersControllers.update)
router.delete('/users/:usersId',isLogedIn,usersControllers.delete);

module.exports = router