require('dotenv').config()
const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const port = process.env.PORT || 6000

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

const authentication = require("./api/routes/authenticationRoutes")
const authors = require("./api/routes/authorRoutes")
const loginstudent = require('./api/routes/loginstudents.Routes')
const publishers = require('./api/routes/publishersRoutes')
const adduser = require('./api/routes/addstudentsRoutes')
const books = require('./api/routes/booksRoutes')
const student = require('./api/routes/studentsRoutes')
const notes = require('./api/routes/notesRoutes')
const borrow = require('./api/routes/borrowRoutes')
const users = require('./api/routes/usersRoutes')
const image = require('./api/routes/imageRoutes')

app.use('/api', books)
app.use('/api', loginstudent)
app.use('/api', authentication)
app.use('/api', authors)
app.use('/api', publishers)
app.use('/api', adduser)
app.use('/api', student)
app.use('/api', notes)
app.use('/api', borrow)
app.use('/api', users)
app.use('/api', image)
app.use(function (req, res) {
    res.status(404).send({ url: req.originalUrl + ' not found' })
})
app.listen(port)

console.log('RESTful API server started on: ' + port); 