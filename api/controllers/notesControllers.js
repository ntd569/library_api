'use strict'

const util = require('util')
const mysql = require('mysql')
const db = require('../configs/db')


module.exports = {
    get: (req, res) => {
        let sql = 'SELECT * FROM notes'
        db.query(sql, (err, response) => {
            if (err) throw err
            res.json(response)
        })
    },
    detail: (req, res) => {
        let sql = 'SELECT * FROM notes WHERE id= ?'
        db.query(sql, [req.params.noteId], (err, response) => {
            if (err) throw err
            res.json(response[0])
        })
    },
    update: (req, res) => {
        let data = req.body;
        let noteId = req.params.noteId;
        let sql = 'UPDATE notes SET ? WHERE id = ?'
        db.query(sql, [data, noteId], (err, response) => {
            if (err) throw err
            res.json({message: 'Update success!'})
        })
    },
    store: (req, res) => {
        let data = req.body;
        let sql = 'INSERT INTO notes SET ?'
        db.query(sql, [data], (err, response) => {
            if (err) throw err
            res.json({message: 'Insert success!'})
        })
    },
    delete: (req, res) => {
        let sql = 'DELETE FROM notes WHERE id = ?'
        db.query(sql, [req.params.noteId], (err, response) => {
            if (err) throw err
            res.json({message: 'Delete success!'})
        })
    }
}