'use strict';
const express = require('express')
const router = express.Router()

let studentControllers = require('../controllers/studentControllers')
const {isLogedIn} =  require("../middlewares/authencationMiddlewares")
const {loginuser} = require('../middlewares/loginstudentsMiddlewares')

router.get('/student/student',loginuser,studentControllers.get)
router.get('/student/student/:studentId',loginuser,studentControllers.detail)

router.get('/admin/student',isLogedIn,studentControllers.get)
router.get('/admin/student/:studentId',isLogedIn,studentControllers.detail)
router.post('/admin/student',isLogedIn,studentControllers.store)
router.put('/student/:studentId',isLogedIn,studentControllers.update)
router.delete('/student/:studentId',isLogedIn,studentControllers.delete);

module.exports = router