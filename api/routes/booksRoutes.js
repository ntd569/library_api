'use strict';
const express = require('express')
const router = express.Router()

let booksControllers = require('../controllers/booksControllers')
const {isLogedIn} =  require("../middlewares/authencationMiddlewares")
const {loginuser} = require('../middlewares/loginstudentsMiddlewares')

router.get('/student/books',loginuser,booksControllers.get)
router.get('/student/books/:bookId',loginuser,booksControllers.detail)

router.get('/admin/books',isLogedIn,booksControllers.get)
router.get('/admin/books/:bookId',isLogedIn,booksControllers.detail)
router.post('/admin/books',isLogedIn,booksControllers.store)
router.put('/books/:bookId',isLogedIn,booksControllers.update)
router.delete('/books/:bookId',isLogedIn,booksControllers.delete);

module.exports = router