'use strict'

const util = require('util')
const mysql = require('mysql')
const db = require('../configs/db')

module.exports = {
    get: (req, res) => {
        let sql = 'SELECT * FROM users'
        db.query(sql, (err, esponse)=>{
            if (err) throw err
            res.json(esponse)
        })
    },
    detail: (req, res) => {
        let sql = 'SELECT * FROM users WHERE id=?'
        let usersId = req.params.usersId;
        db.query(sql, usersId, (err, response) =>{
            if (err) throw err
            res.json(response[0])
        })
    },
    update: (req, res) =>{
        let {userName, password} = req.body;
        let usersId = req.params.usersId;
        let sql = 'UPDATE users SET userName = (?), password = md5(?) WHERE id = (?)'
        db.query(sql, [userName, password, usersId], (err, response)=>{
            console.log(userName, password, usersId)
            if (err) throw err
            res.json({message: 'Update success!'})
        })
    },
    store: (req, res)=>{
        let {userName, password} = req.body;
        let sql = 'INSERT INTO users SET userName = (?), password = md5(?)'
        db.query(sql, [userName, password], (err, response)=>{
            console.log(userName, password)
            if (err) throw err
            res.json({message: 'Insert success!'})
        })
    },
    delete: (req, res) => {
        let sql = 'DELETE FROM users WHERE id =?'
        db.query(sql, [req.params.usersId], (err, response) =>{
            if (err) throw err
            res.json({message: 'Delete success!'})
        })
    }
}