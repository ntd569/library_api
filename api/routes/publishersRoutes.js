'use strict';
const express = require('express')
const router = express.Router()

let publishersContrtoller = require('../controllers/publishersContrtoller')
const {isLogedIn} =  require("../middlewares/authencationMiddlewares")
const {loginuser} = require('../middlewares/loginstudentsMiddlewares')

router.get('/student/publishers',loginuser,publishersContrtoller.get)
router.get('/student/publishers/:publisherId',loginuser,publishersContrtoller.detail)

router.get('/admin/publishers',isLogedIn,publishersContrtoller.get)
router.get('/admin/publishers/:publisherId',isLogedIn,publishersContrtoller.detail)
router.post('/admin/publishers',isLogedIn,publishersContrtoller.store)
router.put('/publishers/:publisherId',isLogedIn,publishersContrtoller.update)
router.delete('/publishers/:publisherId',isLogedIn,publishersContrtoller.delete);

module.exports = router