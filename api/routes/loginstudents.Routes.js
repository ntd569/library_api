let {login} = require('../controllers/loginstudentsController');

const express = require('express')
const router = express.Router()

router.post('/loginstudent', login);

module.exports = router