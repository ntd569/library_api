'use strict'

const util = require('util')
const mysql = require('mysql')
const db = require('../configs/db')

module.exports = {
    get: (req, res) => {
        let sql = 'SELECT * FROM authors name like ?'
        db.query(sql, [ `%${req.query.search ? req.query.search.trim():""}%` ], (err, response) => {
            if (err) throw err
            res.json(response)
        })
    },
    detail: (req, res) => {
        let sql = 'SELECT * FROM authors WHERE id= ?'
        db.query(sql, [req.params.authorId], (err, response) => {
            if (err) throw err
            res.json(response[0])
        })
    },
    update: (req, res) => {
        let data = req.body;
        let authorId = req.params.authorId;
        let sql = 'UPDATE authors SET ? WHERE id = ?'
        db.query(sql, [data, authorId], (err, response) => {
            if (err) throw err
            res.json({message: 'Update success!'})
        })
    },
    store: (req, res) => {
        const data = req.body;
        let sql = 'INSERT INTO authors SET ?'
        db.query(sql, [data], (err, response) => {
            if (err) throw err
            res.json({message: 'Insert success!'})
        })
    },
    delete: (req, res) => {
        let sql = 'DELETE FROM authors WHERE id = ?'
        db.query(sql, [req.params.authorId], (err, response) => {
            if (err) throw err
            res.json({message: 'Delete success!'})
        })
    }
}