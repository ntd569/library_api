const jwt = require('jsonwebtoken');
module.exports.loginuser = (req, res, next) => {
    if (req.headers && req.headers.authorization &&
         String(req.headers.authorization.split(' ')[0]).toLowerCase()=='bearer') {
        var tokenuser = req.headers.authorization.split(' ')[1];
        jwt.verify(tokenuser, process.env.USER_KEY, function(err, decode){
            if(err)
                return res.status(403).send({
                    message:'Token invalid'
                });
            else
                return next();
        });
    } else{
        return res.status(403).send({
            message: 'Unauthorized'
        });
    }
}